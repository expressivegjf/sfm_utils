# -*- encoding: utf-8 -*-

__author__  = "Gavin Falconer (gfalconer@expressivelogic.com)"

def read(*names, **kwargs):
    from io import open
    from os.path import dirname
    from os.path import join
    return open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()
__version__ = read('VERSION').strip()
__author__ = read('AUTHOR').strip()
del read

from .sfm_sniffer import analyse
from .tag_analyser import TagAnalyser


