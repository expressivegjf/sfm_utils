#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""SFM File Sniffer

Usage:
  sfm-sniffer [--tags=<dictionary>] [--summary] [--normal|--stricter|--strictest] <file>
  sfm-sniffer --dumptags
  sfm-sniffer (-h | --help)
  sfm-sniffer --version

Options:
  -t --tags=file  Read a dictionary file that maps tags to labels.
                  If unspecified, the default MDF tag labels will be used.
  -s --summary    Output a summary report only.
  -1 --normal     Apply normal type deduction rules.
  -2 --stricter   Apply stricter type deduction rules.
  -3 --strictest  Apply strictest type deduction rules.
  -d --dumptags   Print the default SFM tag dictionary in the format
                  used by --tags
  --help          Show this screen.
  --version       Show version.

Applying stricter type deduction rules will generate a report that
prefers more specific types (such as 'number' or 'word') over more
general types (such as 'optional text'). However, stricter type
deduction rules are more likely to generate a large number of exceptions.

"""
from __future__ import absolute_import
from __future__ import print_function

# boilerplate to allow running as script directly
# - see https://stackoverflow.com/questions/2943847/nightmare-with-relative-imports-how-does-pep-366-work
if __name__ == "__main__" and __package__ is None:
    import sys, os
    # The following assumes the script is in the top level of the package
    # directory.  We use dirname() to help get the parent directory to add to
    # sys.path, so that we can import the current package.  This is necessary
    # since when invoked directly, the 'current' package is not automatically
    # imported.
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(1, parent_dir)
    import sfm_utils
    __package__ = str("sfm_utils")
    del sys, os

import io
from sfm_utils import __version__
from sfm_utils import __author__
from sfm_utils.sfm_common import default_tag_dictionary
from sfm_utils.sfm_common import read_dict, dump_dict
from sfm_utils.tag_analyser import TagAnalyser


def report_summary(tags):
    """Print a summary report.

    The summary report lists the number of occurrences of each tag
    and the number of value exceptions for each tag, without giving
    details of exceptions.

    Args:
        tags: tag_analyser.TagCollection
            A dictionary-like collection of tags and associated info

    """
    sorted_tags = sorted(tags,
                         key=lambda entry: entry[1]['count'],
                         reverse=True)
    for tag, info in sorted_tags:
        deduced_type = None
        exceptions = 0
        values = info['values']
        if values:
            deduced_type = values.deduced_type
            exceptions = len(values.type_violations)

        print("{tag:4}: {label:20} : occurrences={count:4} : "
              "type={typename:15} : exceptions={ex_count}".format(
                tag=tag,
                label=info['label'],
                count=info['count'],
                typename=deduced_type if deduced_type else "Unknown",
                ex_count=exceptions))


def report(tags):
    """Print a detailed report.

    The detailed report lists the number of occurrences of each tag
    and the number of value exceptions for each tag.
    For tags that have been annotated with example values, lists
    the examples.
    For tags that have associated value exceptions, lists the location
    and the exception value.

    Args:
        tags: tag_analyser.TagCollection
            A dictionary-like collection of tags and associated info

    Side-effects:
        Adds an 'exception_count' field to the information associated
        with each tag.

    """
    # add an exception count to each tag
    for tag, info in tags:
        values = info['values']
        info['exception_count'] = (
            len(values.type_violations) if values else 0
        )

    # tag formatting function
    def print_tag(tag, info):
        deduced_type = None
        examples = None
        exceptions = []
        values = info['values']
        if values:
            deduced_type = values.deduced_type
            exceptions = values.type_violations
            examples = values.examples
        print("{tag:4}: {label:20}: occurrences={count:4}: "
              "type={typename}".format(
                tag=tag,
                label=info['label'],
                count=info['count'],
                typename=deduced_type if deduced_type else "Unknown"))
        if examples:
            print("Example values:")
            print(str(examples))
        if exceptions:
            print("{count} exception{s} "
                  "for {tag} of type '{typename}':".format(
                    tag=tag,
                    count=info['exception_count'],
                    typename=deduced_type,
                    s="s" if info['exception_count'] > 1 else ""))
            for e in exceptions:
                value = e['value'] if e['value'] is not None else "<no value>"
                context = e['context']
                line = context['line_number'] if context else "???"
                print("line {line:4}: {tag} {value}".format(
                        line=line,
                        tag=tag,
                        value=value))

    # sort by descending occurrence
    sorted_tags = sorted(tags, key=lambda t: t[1]['count'], reverse=True)

    # list tags having no exceptions
    no_error_tags = filter(
        lambda t: t[1]['exception_count'] == 0,
        sorted_tags)
    for tag, info in no_error_tags:
        print_tag(tag, info)

    # sort by descending number of exceptions
    sorted_tags = sorted(tags, key=lambda t: t[1]['exception_count'], reverse=True)

    # list tags with exceptions
    error_tags = filter(
        lambda t: t[1]['exception_count'] > 0,
        sorted_tags)
    for tag, info in error_tags:
        print("====================================")
        print_tag(tag, info)


def analyse(filename, tag_dictionary, strictness=1.0, summarize=False):
    """Analyse an SFM file and print the results of the analysis."""
    analyser = TagAnalyser(tag_dictionary, strictness)
    results = analyser.analyse(filename)
    if summarize:
        report_summary(results)
    else:
        report(results)


def main():
    from docopt import docopt

    # parse the command line arguments
    arguments = docopt(__doc__, version=__version__)
    # print(arguments)

    # get the dictionary for tag labels
    tag_dictionary = default_tag_dictionary
    if arguments["--tags"] is not None:
        tag_dictionary = read_dict(arguments["--tags"])

    # interpret arguments
    strictness = 1.0
    if arguments["--stricter"]:
        strictness = 1.5
    if arguments["--strictest"]:
        strictness = 3.5

    filename = arguments["<file>"]
    summarize = arguments["--summary"]

    # execute a command
    if arguments["--dumptags"]:
        dump_dict(tag_dictionary)
    else:
        with (io.open(filename, encoding='utf8')) as sfm:
            analyse(sfm, tag_dictionary, strictness, summarize)

    return 0


if __name__ == '__main__':
    import sys

    if sys.version_info[0] < 3:
        raise Exception("Python 3 or a more recent version is required.")

    sys.exit(main())
