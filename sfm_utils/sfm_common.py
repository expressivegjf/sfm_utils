# -*- encoding: utf-8 -*-
import csv
import io
import sys

# The set of known tags.
# As documented in:
# Making Dictionaries - A guide to lexicography and the
# Multi-Dictionary Formatter : Software version 1.0
# David F. Coward, Charles E. Grimes
# (C) SIL International 2000
# ISBN 1-55671-011-9
default_tag_dictionary = {
    r"\lx": "lexeme",
    r"\hm": "homonym",
    r"\lc": "lexical citation",
    r"\ph": "phonetic form",
    r"\se": "subentry",
    r"\ps": "part of speech",
    r"\pn": "part of speech (national)",
    r"\sn": "sense number",
    r"\gv": "gloss (vernacular)",
    r"\dv": "definition (vernacular)",
    r"\ge": "gloss (english)",
    r"\re": "reversal (english)",
    r"\we": "word-level gloss (english)",
    r"\de": "definition (english)",
    r"\gn": "gloss (national)",
    r"\rn": "reversal (national)",
    r"\wn": "word-level gloss (national)",
    r"\dn": "definition (national)",
    r"\gr": "gloss (regional)",
    r"\rr": "reversal (regional)",
    r"\wr": "word-level gloss (regional)",
    r"\dr": "definition (regional)",
    r"\lt": "literally",
    r"\sc": "scientific name",
    r"\rf": "reference",
    r"\xv": "example (vernacular)",
    r"\xe": "example (english)",
    r"\xn": "example (national)",
    r"\xr": "example (regional)",
    r"\xg": "example (gloss for interlinearizing)",
    r"\ue": "usage (english)",
    r"\un": "usage (national)",
    r"\ur": "usage (regional)",
    r"\uv": "usage (vernacular)",
    r"\ee": "encyclopaedic information (english)",
    r"\en": "encyclopaedic information (national)",
    r"\er": "encyclopaedic information (regional)",
    r"\ev": "encyclopaedic information (vernacular)",
    r"\oe": "only (restrictions - english)",
    r"\on": "only (restrictions - national)",
    r"\or": "only (restrictions - regional)",
    r"\ov": "only (restrictions - vernacular)",
    # todo:
    # r"\lf",
    # r"\le",
    # r"\ln",
    # r"\lr"
    r"\sy": "synonyms",
    r"\an": "antonyms",
    r"\mr": "morphology",
    r"\cf": "cross-reference",
    r"\ce": "cross-reference (english gloss)",
    r"\cn": "cross-reference (national gloss)",
    r"\cr": "cross-reference (regional gloss)",
    r"\mn": "main entry cross-reference",
    r"\va": "variant forms",
    r"\ve": "variant (english comment)",
    r"\vn": "variant (national comment)",
    r"\vr": "variant (regional comment)",
    r"\bw": "borrowed word (loan)",
    r"\et": "etymology (historical)",
    r"\eg": "etymology gloss (english)",
    r"\es": "etymology source",
    r"\ec": "etymology comment",
    r"\pd": "paradigm",
    r"\tb": "table",
    r"\sd": "semantic domain",
    r"\is": "index of semantics",
    r"\th": "thesaurus (vernacular)",
    r"\bb": "bibliographical reference",
    r"\pc": "picture",
    r"\nt": "notes",
    r"\so": "source of data",
    r"\st": "status for editing or printing",
    r"\dt": "date of last edit",
}


def dump_dict(tag_dictionary):
    """Dump the given tag label dictionary to stdout as CSV."""
    writer = csv.writer(sys.stdout)
    writer.writerows(tag_dictionary.items())


def read_dict(filename):
    """Read a tag label CSV file and return the tag label dictionary"""
    tag_dictionary = dict()
    with (io.open(filename, encoding='utf8', newline='')) as f:
        reader = csv.reader(f)
        for row in reader:
            tag = row[0]
            definition = row[1]
            tag_dictionary[tag] = definition
    return tag_dictionary


class _SfmReader(object):
    def __init__(self, sfm_file):
        """A reader object which will iterate over lines in the
        given sfmfile."""
        self._line_num = 0
        self._sfm_file = sfm_file

    def __iter__(self):
        """Return a generator iterator that returns the next
        tag, value pair for each iteration."""
        for i, line in enumerate(self._sfm_file):
            self._line_num = i+1
            tag, value = self._parse_line(line)
            if tag:
                yield tag, value

    def reset(self):
        """Reset the source iterator so that the file can
        be read again.

        Only works if the source iterator is a file-like object,
        supporting seek(). Raises an exception otherwise."""
        # hmmm. may need to find a more generic method for this.
        # seek() is specific to files.
        self._sfm_file.seek(0)
        self._line_num = 0

    @property
    def line_num(self):
        """The number of lines read from the source iterator.
        This is not the same as the number of records returned,
        as records can span multiple lines."""
        return self._line_num

    @staticmethod
    def _parse_line(line):
        """Parse a line from an SFM file into a tag and a value."""
        line = line.strip()
        # ignore blank lines
        if (not line):
            return None, None
        # ignore comment lines
        if (line[0] == '#'):
            return None, None
        # split the line into tag, value parts
        parsed = iter(line.split(maxsplit=1))
        tag = next(parsed, None)
        value = next(parsed, None)
        return tag, value


def sfm_reader(sfm_file):
    """Return a reader object which will iterate over lines in the
    given sfm_file.

    sfm_file can be any object which supports the iterator protocol
    and returns a string each time its __next__() method is called —
    file objects and list objects are both suitable.

    """
    return _SfmReader(sfm_file)
