#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""SFM Structure Sniffer

Usage:
  sfm-struct-sniffer [--tags=<dictionary>] [--deep|--shallow] <file>
  sfm-struct-sniffer --dumptags
  sfm-struct-sniffer (-h | --help)
  sfm-struct-sniffer --version

Options:
  -t --tags=file  Read a dictionary file that maps tags to labels.
                  If unspecified, the default MDF tag labels will be used.
  -d --deep       Perform deep analysis. (default)
  -s --shallow    Perform shallow analysis.
  --dumptags      Print the default SFM tag dictionary in the format
                  used by --tags
  --help          Show this screen.
  --version       Show version.

"""

# boilerplate to allow running as script directly
# - see https://stackoverflow.com/questions/2943847/nightmare-with-relative-imports-how-does-pep-366-work
if __name__ == "__main__" and __package__ is None:
    import sys, os
    # The following assumes the script is in the top level of the package
    # directory.  We use dirname() to help get the parent directory to add to
    # sys.path, so that we can import the current package.  This is necessary
    # since when invoked directly, the 'current' package is not automatically
    # imported.
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(1, parent_dir)
    import sfm_utils
    __package__ = str("sfm_utils")
    del sys, os

import io
from sfm_utils import __version__
from sfm_utils import __author__
from sfm_utils.sfm_common import default_tag_dictionary
from sfm_utils.sfm_common import read_dict, dump_dict
from sfm_utils.tag_structure_analyser import TagStructureAnalyser


def analyse(filename, tag_dictionary, shallow=False):
    """Analyse an SFM file and print the results of the analysis."""
    analyser = TagStructureAnalyser(tag_dictionary)
    policy = TagStructureAnalyser.AnalysisPolicy.DEEP
    if shallow:
        policy = TagStructureAnalyser.AnalysisPolicy.SHALLOW
    analyser.analysis_policy = policy
    with (io.open(filename, encoding='utf8')) as sfm:
        result = analyser.analyse(sfm)
    print("Result:")
    for k in sorted(result):
        if result[k]:
            print("{}:\t{}".format(k, result[k]))


def main():
    from docopt import docopt

    # parse the command line arguments
    arguments = docopt(__doc__, version=__version__)
    print(arguments)

    # get the dictionary for tag labels
    tag_dictionary = default_tag_dictionary
    if arguments["--tags"] is not None:
        tag_dictionary = read_dict(arguments["--tags"])

    # interpret arguments
    filename = arguments["<file>"]
    do_shallow_analysis = arguments["--shallow"]

    # execute a command
    if arguments["--dumptags"]:
        dump_dict(tag_dictionary)
    else:
        analyse(filename, tag_dictionary, do_shallow_analysis)

    return 0


if __name__ == '__main__':
    import sys

    if sys.version_info[0] < 3:
        raise Exception("Python 3 or a more recent version is required.")

    sys.exit(main())
