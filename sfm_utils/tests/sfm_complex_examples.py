# -*- encoding: utf-8 -*-

"""
Complex example demonstrating the behaviour when repeated tags are
nested and also some repeated tags are not nested.

Tags that are not nested may precede the first enclosing element.
Tags that are not nested may trail after the last enclosing element.
"""
complex_sfm_with_trailing_repeating_tags = {
'sfm': r"""

\lx lexeme 1
\rpt non-nested repeating tag before nested tag
\rpt non-nested repeating tag before nested tag
\ps enclosing tag 1
\ge english
\gn national
\gn national 2
\gr regional
\ps enclosing tag 2
\ge english
\gn national
\gn national 2
\gr regional
\opt optional tag trailing after enclosed section

\lx lexeme 2
\ps enclosing tag 1
\ge english
\gn national
\gn national 2
\gr regional
\ps enclosing tag 2
\ge english
\gn national
\gn national 2
\gr regional
\rpt non-nested repeating tag trailing enclosed section
\rpt non-nested repeating tag trailing enclosed section
# missing \opt optional tag trailing after enclosed section

\lx lexeme 2
\rpt non-nested repeating tag before nested tag
\rpt non-nested repeating tag before nested tag
\ps enclosing tag single instance
\ge english
# missing \gn national
\gr regional
\opt optional tag trailing after enclosed section

""",

'schema': {
 'ROOT':          { '\\lx' },
 'ROOT\\lx':      { '\\rpt', '\\opt', '\\ps' },
 'ROOT\\lx\\ps':  { '\\gn', '\\ge', '\\gr' },
},

'syntax': r"""

\lx+
    \rpt[2]
    \opt?
    \ps+
        \ge
        \gn*
        \gr

"""
}


"""
Complex example demonstrating the behaviour with multiple types of
enclosing tag in a sequence.

"""
complex_sfm_with_multiple_nested_tag_types = {
'sfm': r"""

\lx lexeme 1

\rpt enclosing tag type 2 instance 1
\sub subtag 1
\rpt enclosing tag type 2 instance 2
\sub subtag 1

\ps enclosing tag 1
\ge english
\gn national
\gn national 2
\gr regional
\ps enclosing tag 2
\ge english
\gn national
\gn national 2
\gr regional

\opt optional tag trailing after enclosed section


\lx lexeme 2

\ps enclosing tag 1
\ge english
\gn national
\gn national 2
\gr regional
\ps enclosing tag 2
\ge english
\gn national
\gn national 2
\gr regional

\rpt enclosing tag type 2 instance 1
\sub subtag 1
\rpt enclosing tag type 2 instance 2
\sub subtag 1

# missing \opt optional tag trailing after enclosed section


\lx lexeme 3

\rpt enclosing tag type 2 instance 1
\sub subtag 1
\rpt enclosing tag type 2 instance 2
\sub subtag 1

\ps enclosing tag single instance
\ge english
# missing \gn national
\gr regional

\opt optional tag trailing after enclosed section

""",

'schema': {
 'ROOT':          { '\\lx' },
 'ROOT\\lx':      { '\\rpt', '\\opt', '\\ps' },
 'ROOT\\lx\\rpt': { '\\sub' },
 'ROOT\\lx\\ps':  { '\\gn', '\\ge', '\\gr' },
},

'syntax': r"""

\lx+
    \opt?
    \rpt[2]
        \sub
    \ps+
        \ge
        \gn*
        \gr

"""
}


"""
Complex example demonstrating the behaviour with alternate nested tag
types.

Representative of an sfm file that sometimes uses \ps as the enclosing
tag for parts of a lexeme, and sometimes uses \sn.

"""
complex_sfm_with_alternate_nested_tag_types = {
'sfm': r"""

\lx lexeme 1

\sn sense number 1
\ps verb
\ge english
\gn national
\gn national 2
\gr regional
\sn sense number 2
\ps adverb
\ge english
\gn national
\gn national 2
\gr regional

\opt optional tag trailing after enclosed section


\lx lexeme 2

\ps verb
\sn sense number 1
\ge english
\gn national
\gn national 2
\gr regional
\ps adverb
\sn sense number 2
\ge english
\gn national
\gn national 2
\gr regional

# missing \opt optional tag trailing after enclosed section


\lx lexeme 3

\ps verb
\sn sense number 1
\ge english
# missing \gn national
\gr regional

\opt optional tag trailing after enclosed section


\lx lexeme 4

\sn sense number 1
\ps verb
\ge english
\gn national
\gn national 2
# missing \gr regional

# missing \opt optional tag trailing after enclosed section

""",

'schema': {
 'ROOT':          { '\\lx' },
 'ROOT\\lx':      { '\\opt', '\\sn', '\\ps' },
 'ROOT\\lx\\sn':  { '\\ps', '\\gn', '\\ge', '\\gr' },
 'ROOT\\lx\\ps':  { '\\sn', '\\gn', '\\ge', '\\gr' },
},

'syntax': r"""

\lx+
    \opt?
    \sn*
        \ps
        \ge
        \gn[2]
        \gr?
    \ps*
        \sn
        \ge
        \gn*
        \gr

"""
}



"""
Complex example demonstrating the behaviour with repeated tags
in the trailing part of the file.

Representative of an sfm file that sometimes uses \ps as the enclosing
tag for parts of a lexeme, and sometimes uses \sn.

"""
complex_sfm_with_repeated_trailing_tags = {
'sfm': r"""

\lx lexeme 1

\sn sense number 1.1
\ps verb
\ge english
\gn national 1.1.1
\gn national 1.1.2
\gr regional
\sn sense number 1.2
\ps adverb
\ge english
\gn national 1.2.1
\gn national 1.2.2
\gr regional

\opt optional tag trailing after enclosed section


\lx lexeme 2

\ps verb
\sn sense number 2.1
\ge english
\gn national 2.1.1
\gn national 2.1.2
\gr regional
\ps adverb
\sn sense number 2.2
\ge english
\gn national 2.2.1
\gn national 2.2.2
\gr regional

# missing \opt optional tag trailing after enclosed section


\lx lexeme 3

\ps verb
\sn sense number 3.1
\ge english
# missing \gn national
\gr regional

# missing \opt optional tag trailing after enclosed section


\lx lexeme 4

\sn sense number 4.1
\ps verb
\ge english  4.1_1
\gn national 4.1_1.1
\gn national 4.1_1.2
\gr regional
\ge english 4.1_2
\gn national 4.1_2.1
\gn national 4.1_2.2
# missing \gr regional 2

\opt optional tag trailing after enclosed section

""",

'schema': {
 'ROOT':              { '\\lx' },
 'ROOT\\lx':          { '\\opt', '\\sn', '\\ps' },
 'ROOT\\lx\\sn':      { '\\ps', '\\ge' },
 'ROOT\\lx\\sn\\ge':  { '\\gn', '\\gr' },
 'ROOT\\lx\\ps':      { '\\sn', '\\gn', '\\ge', '\\gr' },
},

'syntax': r"""

\lx+
    \opt?
    \sn*
        \ps
        \ge
            \gn[2]
            \gr?
    \ps*
        \sn
        \ge
        \gn*
        \gr

"""
}
