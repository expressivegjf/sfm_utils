import pytest
from sfm_simple_examples import *
from sfm_complex_examples import *
from ..tag_structure_analyser import TagStructureAnalyser


@pytest.mark.parametrize("example", [
    simple_sfm,
    sfm_with_optional_tags,
    sfm_with_repeating_tags,
    sfm_with_nested_tags,
    sfm_with_optional_nested_tags,
    complex_sfm_with_trailing_repeating_tags,
    complex_sfm_with_multiple_nested_tag_types,
    complex_sfm_with_alternate_nested_tag_types,
    complex_sfm_with_repeated_trailing_tags,
])
def test_sfm_schema(example):
    analyser = TagStructureAnalyser()
    analyser.analysis_policy = TagStructureAnalyser.AnalysisPolicy.SHALLOW
    sfm_file = example['sfm'].splitlines()
    result = analyser.analyse(sfm_file)
    assert result == example['schema']


def test_parse_tree():
    analyser = TagStructureAnalyser()
    schema = {
        'ROOT':             { '\lx' },
        'ROOT\lx':          { '\ps', '\misc' },
        'ROOT\lx\ps':       { '\ge', '\gr', '\gn' },
        'ROOT\lx\ps\ge':    { '\misc' },
    }
    elements = [
    '\lx',
        '\misc',
        '\ps',
            '\ge',
                '\misc',
            '\gr',
            '\gr',
            '\gr',
            '\gn',
        '\ps',
            '\gr',
            '\ge',
            '\ge',
                '\misc',
            '\ge',
        '\misc',
    ]

    expected = [
    '\\lx', [
        '\\misc',
        '\\ps', [
            '\\ge', [
                '\\misc'
                ],
            '\\gr',
            '\\gr',
            '\\gr',
            '\\gn'
            ],
        '\\ps', [
            '\\gr',
            '\\ge', [
                ],
            '\\ge', [
                '\\misc'
                ],
            '\\ge', [
                '\\misc'
                ]
            ]
        ]
    ]
    result = analyser._parse_tree(elements, schema)
    assert result == expected


def test_flatten_tree():
    analyser = TagStructureAnalyser()
    tree = [['\\lx', '\\misc', ['\\ps', ['\\ge', '\\misc'], '\\gr', '\\gr', '\\gr', '\\gn'], '\\misc']]
    result = analyser._flatten_tree(tree)
    assert len(result) == 10
    assert result == ['\\lx', '\\misc', '\\ps', '\\ge', '\\misc', '\\gr', '\\gr', '\\gr', '\\gn', '\\misc']
