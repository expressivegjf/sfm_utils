# -*- encoding: utf-8 -*-

simple_sfm = {
'sfm': r"""

\lx first lexeme
\ps adv
\ge english
\gn national
\gr regional
\glo other
\glv vernacular

\lx second lexeme
\ps adv
\ge english
\gn national
\gr regional
\glo other
\glv vernacular

\lx third lexeme
\ps prp
\ge english
\gn national
\gr regional
\glo other
\glv vernacular

""",

'schema': {
 'ROOT':      { '\\lx' },
 'ROOT\\lx':  { '\\ps', '\\gn', '\\ge', '\\gr', '\\glo', '\\glv' },
},

'syntax': r"""

\lx+
    \ps
    \gn
    \ge
    \gr
    \glo
    \glv

"""
}


sfm_with_optional_tags = {

'sfm': r"""

\lx first lexeme
\ps adv
\ge english
\gn national
#  missing \gr regional
\glo other
\glv vernacular

\lx second lexeme
\ps adv
\ge english
\gn national
\gr regional
\glo other
\glv vernacular

\lx third lexeme
\ps prp
\ge english
\gn national
\gr regional
\glo other
#  missing \glv vernacular

""",

'schema': {
 'ROOT':      { '\\lx' },
 'ROOT\\lx':  { '\\ps', '\\gn', '\\ge', '\\gr', '\\glo', '\\glv' },
},

'syntax': r"""

\lx+
    \ps
    \ge
    \gn
    \gr?
    \glo
    \glv?

"""
}

"""
Example demonstrating the behaviour when tags are repeated, without
nesting.

If a tag is always repeated the same number of times,
it is annotated with a rank '[n]', e.g. \gn[2]

If a tag is always present one or more times,
it is annotated with '+', e.g. \ge+

If a tag is present zero or more times,
it is annotated with '*', e.g. \glv*

"""
sfm_with_repeating_tags = {
'sfm': r"""

\lx lexeme 1
\ps adv
\ge english repeated 1
\ge english repeated 2
\ge english repeated 3
\ge english repeated 4
\gn national repeated 1
\gn national repeated 2
\gr regional
\glo other
#  mising \glv vernacular

\lx lexeme 2
\ps adv
\ge english repeated 1
\ge english repeated 2
\ge english repeated 3
\gn national repeated 1
\gn national repeated 2
#  missing \gr regional
\glo other
\glv vernacular repeated 1
\glv vernacular repeated 2

\lx lexeme 3
\ps prp
\ge english repeated 1
\ge english repeated 2
\ge english repeated 3
\gn national repeated 1
\gn national repeated 2
\gr regional
\glo other
\glv vernacular

""",

'schema': {
 'ROOT':      { '\\lx' },
 'ROOT\\lx':  { '\\ps', '\\gn', '\\ge', '\\gr', '\\glo', '\\glv' },
},

'syntax': r"""

\lx+
    \ps
    \ge+
    \gn[2]
    \gr?
    \glo
    \glv*

"""
}

"""
Example demonstrating the behaviour when tags are nested 2 levels deep,
but still with a simple repeating pattern.

"""
sfm_with_nested_tags = {
'sfm': r"""

\lx ágashe

\ps adv
\ge outside
\gn dehors
\gr

\ps adj
\ge outside
\gn dehors
\gr

\glo á bəra
\glv geshke

\lx áhuna

\ps adv
\gn ici
\ge here
\gr haa ɗo

\ps adj
\gn ici
\ge here
\gr haa ɗo

\glo ́ákinene
\glv akəna

\lx átevge

\ps prp
\gn à côté de
\ge
\gr

\glo ákà
\glo ákà
\glo ákà
\glv

""",

'schema': {
 'ROOT':          { '\\lx' },
 'ROOT\\lx':      { '\\ps', '\\glo', '\\glv' },
 'ROOT\\lx\\ps':  { '\\gn', '\\ge', '\\gr' },
},

'syntax': r"""

\lx+
    \ps+
        \gn
        \ge
        \gr
    \glo*
    \glv?

"""
}


"""
Example demonstrating the behaviour when tags are nested more than one
level deep, and a nesting tag is itself optional.

"""
sfm_with_optional_nested_tags = {
'sfm': r"""

\lx without nested \ps element

\lx ágashe

\ps adv
\ge outside
\gn dehors
\gr

\ps adj
\ge outside
\gn dehors
\gr

\glo á bəra
\glv geshke

\lx áhuna

\ps adv
\gn ici
\ge here
\gr haa ɗo

\ps adj
\gn ici
\ge here
\gr haa ɗo

\glo ́ákinene
\glv akəna

\lx átevge

\ps prp
\gn à côté de
\ge
\gr

\glo ákà
\glo ákà
\glo ákà
\glv

""",

'schema': {
 'ROOT':          { '\lx' },
 'ROOT\\lx':      { '\\ps', '\\glo', '\\glv' },
 'ROOT\\lx\\ps':  { '\\gn', '\\ge', '\\gr' },
},

'syntax': r"""

\lx+
    \ps*
        \gn
        \ge
        \gr
        \glo*
        \glv?

"""
}
