import pytest
from tag_analyser import TypeDeducingValueCollectionFactory as Tdvcf

def test_is_number():
    """Test is_number evaluation."""
    assert Tdvcf.is_number("123") == True
    assert Tdvcf.is_number("0") == True
    assert Tdvcf.is_number("-1") == False  # expected this to be true!
    assert Tdvcf.is_number("A") == False
    assert Tdvcf.is_number("A1") == False
    assert Tdvcf.is_number("1A") == False
    assert Tdvcf.is_number("123.456") == False
    assert Tdvcf.is_number("1.2.3") == False

def test_is_word():
    """Test is_word regex evaluation."""
    assert Tdvcf.is_word("foo") == True
    assert Tdvcf.is_word("foo.") == True
    assert Tdvcf.is_word("!foo.") == True
    assert Tdvcf.is_word("`foo`bar'") == True
    assert Tdvcf.is_word("foo bar") == False
    assert Tdvcf.is_word("foo.bar") == False
    assert Tdvcf.is_word("foo..") == False
    assert Tdvcf.is_word("foo.,") == True
    assert Tdvcf.is_word("foo.;") == True
    assert Tdvcf.is_word("foo,.") == False
    assert Tdvcf.is_word("foo;.") == False
    assert Tdvcf.is_word("foo,") == True
    assert Tdvcf.is_word("foo;") == True
    assert Tdvcf.is_word("foo  ,  ") == True
    assert Tdvcf.is_word("foo  ;  ") == True
    assert Tdvcf.is_word("foo,bar") == False
    assert Tdvcf.is_word("foo;bar") == False
    assert Tdvcf.is_word("!") == False
    assert Tdvcf.is_word("`") == False
    assert Tdvcf.is_word("漢字") == True
    assert Tdvcf.is_word("漢字,") == True
    assert Tdvcf.is_word("漢字;") == True
    assert Tdvcf.is_word("漢 字") == False
    assert Tdvcf.is_word("漢,字") == False
    assert Tdvcf.is_word("漢;字") == False

def test_is_single_term():
    """Test is_single_term regex evaluation."""
    assert Tdvcf.is_single_term("foo") == True
    assert Tdvcf.is_single_term("foo.") == True
    assert Tdvcf.is_single_term("!foo.") == True
    assert Tdvcf.is_single_term("`foo`bar'") == True
    assert Tdvcf.is_single_term("foo bar") == True
    assert Tdvcf.is_single_term("foo   bar    baz") == True
    assert Tdvcf.is_single_term("foo.bar") == False
    assert Tdvcf.is_single_term("foo..") == False
    assert Tdvcf.is_single_term("foo.,") == True
    assert Tdvcf.is_single_term("foo.  ;") == True
    assert Tdvcf.is_single_term("foo,.") == False
    assert Tdvcf.is_single_term("foo;.") == False
    assert Tdvcf.is_single_term("foo,  ") == True
    assert Tdvcf.is_single_term("foo;") == True
    assert Tdvcf.is_single_term("foo  ,  ") == True
    assert Tdvcf.is_single_term("foo  ;  ") == True
    assert Tdvcf.is_single_term("foo,bar") == False
    assert Tdvcf.is_single_term("foo; bar") == False
    assert Tdvcf.is_single_term("!") == False
    assert Tdvcf.is_single_term("`") == False
    assert Tdvcf.is_single_term("漢字") == True
    assert Tdvcf.is_single_term("漢字,") == True
    assert Tdvcf.is_single_term("漢 字") == True
    assert Tdvcf.is_single_term("漢, 字") == False
    assert Tdvcf.is_single_term("漢;字") == False
