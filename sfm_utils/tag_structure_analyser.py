#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

from sfm_utils.sfm_common import sfm_reader
from collections import defaultdict
from enum import Enum


class TagStructureAnalyser(object):

    class AnalysisPolicy(Enum):
        SHALLOW = 0
        DEEP    = 1


    def __init__(self, tag_dictionary=None):
        super(TagStructureAnalyser, self).__init__()
        self._policy = TagStructureAnalyser.AnalysisPolicy.DEEP

    @property
    def analysis_policy(self):
        return self._policy

    @analysis_policy.setter
    def analysis_policy(self, value):
        self._policy = value

    def analyse(self, sfm_file, shallow=False):
        # read the sfm_file to collect occurrences of each tag
        sfm = sfm_reader(sfm_file)
        tags = [tag for tag, value in sfm]

        # analyse the sfm file to determine the tree structure
        return self._analyse_tree("ROOT", [tags])

    def _analyse_tree(self, element_type, fragments, schema=None):
        """Do stuff to analyse the tree.

        :param: tag_type  - the type of tag being analysed.
        :param: fragments - a list of tree fragments. A fragment is a list of tags.

        :returns: a list of tag types that have been identified.
        """
        if schema is None:
            schema = defaultdict(set)

        # find the set of candidate partitioning elements under this element type
        candidates = self._find_candidate_partition_elements(fragments)
        print("Candidates for {}: {}".format(element_type, candidates))

        # partition each fragment into a tuple comprising:
        # ( preamble, [list of fragments], remainder)
        preambles = []
        repeated = defaultdict(list)
        postscripts = defaultdict(list)
        for fragment in fragments:
            p, parts = self._partition(candidates, fragment)
            preambles.extend(parts[0])
            if p is not None:
                repeated[p].extend(parts[1])
                postscripts[p].append(parts[2])

        # initialise the schema
        schema[element_type].update(preambles)
        schema[element_type].update(repeated.keys())
        print ("Initial schema for {}: {}".format(element_type, schema[element_type]))

        #
        # TODO - given the set of structural elements and splits,
        # what is the optimal order to process them in?
        #

        for p in sorted(repeated):      # sorted to ensure deterministic outcome for simpler testing/debugging
            ptype = element_type + p
            print("Analysing {} for {}".format(ptype, repeated[p]))
            analysis = (self._analyse_tree(ptype, repeated[p]))
            # merge existing analysis into schema
            for element in analysis:
                schema[element].update(analysis[element])
            print ("Schema after {}: {}".format(ptype, schema))

        # consume non-enclosed (trailing) subtrees

        #====================
        # TODO - if schema changed, iterate
        #
        # What if there are repeating patterns in the remainders...?
        #
        remainders = []
        for p in postscripts:
            redo = False
            ptype = element_type + p
            print("Parsing {} for {}".format(ptype, postscripts[p]))
            for fragment in postscripts[p]:
                contained = self._consume(fragment, schema, ptype)
                print("Parsed {} for {}: {}".format(contained, ptype, fragment))
                if contained > 0:
                    repeated[p].append(fragment[:contained])
                    redo = True

                # analyse the parsed portion in case of new repeated patterns
                # analysis = self._analyse_tree(ptype, [fragment[:contained]])
                # merge into existing schema
                # for element in analysis:
                #     schema[element].update(analysis[element])
                # print ("** Updated schema for {}: {}".format(ptype, schema))

                if contained < len(fragment):
                    remainders.append(fragment[contained:])
            if redo and self._policy == TagStructureAnalyser.AnalysisPolicy.DEEP:
                print("Re-running for {}!".format(ptype))
                analysis = self._analyse_tree(ptype, repeated[p])
                for element in analysis:
                    schema[element] = analysis[element]
                print("Updated schema after {}: {}".format(ptype, schema))

        print("Remainders for {}: {}".format(element_type, remainders))
        # analyse the remainders, using the existing analysis
        if remainders:
            schema = self._analyse_tree(element_type, remainders, schema)
        #
        #======================

        return schema


    @staticmethod
    def _find_candidate_partition_elements(fragments):
        repeated = (
            TagStructureAnalyser._find_first_repeating_element(f) for f in fragments
        )
        result = set(repeated)
        result.discard(None)
        return result


    @staticmethod
    def _partition(partitioning, elements):
        # select a partitioning element
        p = TagStructureAnalyser._find_first(partitioning, elements)
        if p is None:
            # special case - nothing to do
            return None, (elements, [], [])

        # create a list of the indices of the partitioning element
        splits = [ i for i, e in enumerate(elements) if e == p ]

        # grab the preamble
        i = splits[0]
        preamble = elements[:i]

        # grab the repeating fragments
        repeated = [
            elements[i+1:j] for i, j in zip(splits[:-1], splits[1:])
        ]

        # grab the remainder
        i = splits[-1] + 1
        remainder = elements[i:]

        # return the tuple of results
        return p, (preamble, repeated, remainder)

    @staticmethod
    def _consume(elements, schema, schema_root="ROOT"):
        parsed = TagStructureAnalyser._parse_tree(elements, schema, schema_root)
        return len(TagStructureAnalyser._flatten_tree(parsed))

    @staticmethod
    def _flatten_tree(l, ltypes=(list, tuple)):
        ltype = type(l)
        l = list(l)
        i = 0
        while i < len(l):
            while isinstance(l[i], ltypes):
                if not l[i]:
                    l.pop(i)
                    i -= 1
                    break
                else:
                    l[i:i + 1] = l[i]
            i += 1
        return ltype(l)

    @staticmethod
    def _parse_tree(elements, schema, schema_root="ROOT"):
        parsed = []
        if not elements:
            return parsed
        e = elements[0]
        if e not in schema[schema_root]:
            return parsed
        if schema_root + e in schema:
            parsed.append(e)
            parsed.append(TagStructureAnalyser._parse_tree(elements[1:], schema, schema_root+e))
            consumed = len(TagStructureAnalyser._flatten_tree(parsed));
            # print("Extending for {} after {}: {}".format(schema_root, consumed, elements[consumed:]))
            parsed.extend(TagStructureAnalyser._parse_tree(elements[consumed:], schema, schema_root))
        else:
            parsed.extend([e] + TagStructureAnalyser._parse_tree(elements[1:], schema, schema_root))
        return parsed

    @staticmethod
    def _find_first(candidates, elements):
        if not candidates:
            return None
        if not elements:
            return None
        finder = (e for e in elements if e in candidates)
        return next(finder, None)

    @staticmethod
    def _find_first_repeating_element(elements):
        if not elements:
            return None
        # generate a dictionary that stores the indices for each
        # element:
        # { '\lx': [0, 3, 7],  '\ps': [1, 4],  ... }
        element_indices = defaultdict(list)
        for i, e in enumerate(elements[:-1]):
            # ignore consecutive runs of the same element;
            # store only the last occurrence within a consecutive run
            e_next = elements[i+1]
            if e != e_next:
                element_indices[e].append(i)
        # store the final element
        e = elements[-1]
        i = len(elements)-1
        element_indices[e].append(i)

        # find the earliest repeated element
        repeated = (k for k in element_indices if len(element_indices[k]) > 1)
        element = min(repeated,
                      key=lambda k: element_indices[k][0],  # select by index of first occurrence
                      default=None)
        return element
