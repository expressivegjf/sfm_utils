# SFM Structure Sniffer
## Functional Specification

### Overview
SFM Structure Sniffer is a fast command-line utility to identify structural
problems with an SFM file that would cause errors when importing the file
into SIL FieldWorks Language Explorer (FLEX).

### Background
#### SFM file structure

#### FLEX import procedure

### Scenarios
(using personas)

### Non-Goals

...
